package com.hcl.employeemanagement.exception;

import java.util.Date;

public class ErrorResponse {

	private Date timestamp;
	private String message;
	private String detailes;
	private String httpCodeMessage;

	public ErrorResponse() {
		super();
	}

	public ErrorResponse(Date timestamp, String message, String detailes, String httpCodeMessage) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.detailes = detailes;
		this.httpCodeMessage = httpCodeMessage;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetailes() {
		return detailes;
	}

	public void setDetailes(String detailes) {
		this.detailes = detailes;
	}

	public String getHttpCodeMessage() {
		return httpCodeMessage;
	}

	public void setHttpCodeMessage(String httpCodeMessage) {
		this.httpCodeMessage = httpCodeMessage;
	}

}
