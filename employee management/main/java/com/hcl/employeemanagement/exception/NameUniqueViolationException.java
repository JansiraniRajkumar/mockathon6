package com.hcl.employeemanagement.exception;

public class NameUniqueViolationException extends RuntimeException {
	

	private static final long serialVersionUID = 1L;

	public NameUniqueViolationException(String message) {
		super(message);
	}


}
