package com.hcl.employeemanagement.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.employeemanagement.dao.EmployeeRespositroy;
import com.hcl.employeemanagement.dto.EmployeeDto;
import com.hcl.employeemanagement.dto.ResponseMessage;
import com.hcl.employeemanagement.exception.EmployeeNotFoundException;
import com.hcl.employeemanagement.model.Employee;



@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRespositroy employeeRespositroy;
	Employee employee = new Employee();
	ResponseMessage dto = new ResponseMessage();

	@Override
	public ResponseMessage createEmployee(EmployeeDto employeedto) {
		BeanUtils.copyProperties(employeedto, employee);
		employee = employeeRespositroy.save(employee);
		dto.setMessage("create successfully");
		return dto;

	}

	@Override
	public Employee updateEmployee(EmployeeDto employeedto) {
		BeanUtils.copyProperties(employeedto, employee);
		Employee employee1 = employeeRespositroy.save(employee);
		return employee1;
	}

	@Override
	public ResponseMessage deleteEmployee(int employeeCode) {
		employeeRespositroy.deleteById(employeeCode);
		dto.setMessage("deleted successfully");
		return dto;
	}

	@Override
	public Employee getByEmployee(int employeeCode) {
		Optional<Employee> option = employeeRespositroy.findById(employeeCode);
		Employee employee = null;
		if (option.isPresent()) {
			employee = option.get();

		} else {
			throw new EmployeeNotFoundException("employee with id: " + employeeCode + "is not found");
		}
		return employee;
	}

	@Override
	public List<Employee> getAllEmployess() {
		return employeeRespositroy.findAll();
	}

}
