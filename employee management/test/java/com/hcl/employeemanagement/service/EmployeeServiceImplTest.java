package com.hcl.employeemanagement.service;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;

import com.hcl.employeemanagement.dao.EmployeeRespositroy;
import com.hcl.employeemanagement.dto.EmployeeDto;
import com.hcl.employeemanagement.dto.ResponseMessage;
import com.hcl.employeemanagement.model.Employee;




@RunWith(MockitoJUnitRunner.Silent.class)
public class EmployeeServiceImplTest {
	

	@InjectMocks
	EmployeeServiceImpl employeeServiceImpl;

	@Mock
	EmployeeRespositroy employeeRepository;
	
	@Test
	public void testGetEmployeesForPositive() {
		List<Employee> employees = new ArrayList<Employee>();
		Employee employee=new Employee(5190, "jansi", "db", "jansi@gmail.com");
		employees.add(employee);
		Employee employee1=new Employee(5192, "jenni","db", "jenni@gmail.com");
		employees.add(employee1);
		Mockito.when(employeeRepository.findAll()).thenReturn(employees);
		List<Employee> employee2 = employeeServiceImpl.getAllEmployess();
		Assert.assertNotNull(employee2);
		Assert.assertEquals(2, employee2.size());

	}

	@Test
	public void testGetEmployeesForNegative() {
		List<Employee> employees = new ArrayList<Employee>();
		Employee employee=new Employee(-5190, "jansi", "db", "jansi@gmail.com");
		employees.add(employee);
		Employee employee1=new Employee(-5192, "jenni","db", "jenni@gmail.com");
		employees.add(employee1);
		Mockito.when(employeeRepository.findAll()).thenReturn(employees);
		List<Employee> employee2 = employeeServiceImpl.getAllEmployess();
		Assert.assertNotNull(employee2);
		Assert.assertEquals(2, employee2.size());

	}

	@Test
	public void testFindByIdForPositive() {
		Employee employee=new Employee(5190, "jansi", "db", "jansi@gmail.com");
		Mockito.when(employeeRepository.findById(5190)).thenReturn(Optional.of(employee));
	employeeServiceImpl.getByEmployee(5190);
		Assert.assertNotNull(employee);
		Assert.assertEquals("db", employee.getProjectName());
	}


	@Test
	public void testFindByIdForNegative() {
		Employee employee=new Employee(-5190, "jansi","db", "jansi@gmail.com");
		Mockito.when(employeeRepository.findById(-5190)).thenReturn(Optional.of(employee));
 employeeServiceImpl.getByEmployee(-5190);
		Assert.assertNotNull(employee);
		Assert.assertEquals("db", employee.getProjectName());
	}
	

	


	@Test
	public void testCreateEmployeeForPositive() {
		Employee employee=new Employee(5190, "jansi", "db", "jansi@gmail.com");
		EmployeeDto dto=new EmployeeDto(5190, "jansi","db", "jansi@gmail.com");
		BeanUtils.copyProperties(employee, dto);
		ResponseMessage message=new ResponseMessage();
		message.setMessage("created successfully");
		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Assert.assertNotEquals(message, employeeServiceImpl.createEmployee(dto));
	}


	@Test
	public void testCreateEmployeeForNegative() {
		Employee employee=new Employee(-5190, "jansi", "db", "jansi@gmail.com");
		EmployeeDto dto=new EmployeeDto(5190, "jansi","db", "jansi@gmail.com");
		ResponseMessage message=new ResponseMessage();
		message.setMessage("created successfully");
		BeanUtils.copyProperties(employee, dto);
		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Assert.assertNotEquals(message, employeeServiceImpl.createEmployee(dto));
	}


	@Test
	public void removeEmployeeForPositive() {
		new Employee(5190, "jansi", "db", "jansi@gmail.com");
		employeeServiceImpl.deleteEmployee(5190);
		verify(employeeRepository, times(1)).deleteById(5190);
	}

	@Test
	public void removeEmployeeForNegative() {
		new Employee(-5190, "jansi", "db", "jansi@gmail.com");
		employeeServiceImpl.deleteEmployee(-5190);
		verify(employeeRepository, times(1)).deleteById(-5190);
	}



	@Test
	public void testupdateEmployeeForPositive() {
		Employee employee=new Employee();
		EmployeeDto dto=new EmployeeDto(5190, "jansi","db", "jansi@gmail.com");
		BeanUtils.copyProperties(employee, dto);
		ResponseMessage message=new ResponseMessage();
		message.setMessage("created successfully");
		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employee1=employeeServiceImpl.updateEmployee(dto);
		Assert.assertEquals(employee1, employeeServiceImpl.updateEmployee(dto));
	}

	@Test
	public void testupdateEmployeeForNegative() {
		Employee employee=new Employee(5190, "jansi", "db", "jansi@gmail.com");
		EmployeeDto dto=new EmployeeDto(5190, "jansi","db", "jansi@gmail.com");
		BeanUtils.copyProperties(employee, dto);
		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employee1=employeeServiceImpl.updateEmployee(dto);
		Assert.assertEquals(employee1, employeeServiceImpl.updateEmployee(dto));
	}
	

}
