package com.hcl.employeemanagement.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.employeemanagement.dto.EmployeeDto;
import com.hcl.employeemanagement.dto.ResponseMessage;
import com.hcl.employeemanagement.model.Employee;
import com.hcl.employeemanagement.service.EmployeeService;




@RunWith(MockitoJUnitRunner.class)
public class EmployeeControllerTest {
	
	@InjectMocks
	EmployeeController employeeeController;

	@Mock
	EmployeeService employeeService;
	
	@Test
	public void testEmployeeByIdForPositive() {
		Employee employee=new Employee(5190, "jansi", "db", "jansi@gmail.com");
		Mockito.when(employeeService.getByEmployee(5190)).thenReturn(employee);
		ResponseEntity<Employee> employee1 = employeeeController.getEmployee(5190);
		Assert.assertNotNull(employee1);
		Assert.assertEquals(HttpStatus.OK, employee1.getStatusCode());

	}

	@Test
	public void testStudentByIdForNegative() {
		Employee employee=new Employee(-5190, "jansi", "db", "jansi@gmail.com");
		Mockito.when(employeeService.getByEmployee(-5190)).thenReturn(employee);
		ResponseEntity<Employee> employee1 = employeeeController.getEmployee(-5190);
		Assert.assertNotNull(employee1);
		Assert.assertEquals(HttpStatus.OK, employee1.getStatusCode());

	}

	@Test
	public void testDeleteStudentForPositive() {
		new Employee(5190, "jansi", "db", "jansi@gmail.com");
		employeeService.deleteEmployee(5190);
		ResponseEntity<ResponseMessage> employee1 = employeeeController.deleteEmployess(5190);
		Assert.assertNotNull(employee1);
		Assert.assertEquals(HttpStatus.OK, employee1.getStatusCode());

	}


	@Test
	public void testDeleteStudentForNegative() {
		new Employee(-5190, "jansi", "db", "jansi@gmail.com");
		employeeService.deleteEmployee(-5190);
		ResponseEntity<ResponseMessage> employee1 = employeeeController.deleteEmployess(5190);
		Assert.assertNotNull(employee1);
		Assert.assertEquals(HttpStatus.OK, employee1.getStatusCode());

	}

	@Test
	public void testGetAllStudentForPositive() {

		List<Employee> employees = new ArrayList<Employee>();
		Employee employee=new Employee(5190, "jansi", "db", "jansi@gmail.com");
		employees.add(employee);
		Mockito.when(employeeService.getAllEmployess()).thenReturn(employees);
		ResponseEntity<List<Employee>> re = employeeeController.getAllEmployess();
		Assert.assertNotNull(re);
		Assert.assertEquals(HttpStatus.OK, re.getStatusCode());

	}
	@Test
	public void testGetAllStudentForNegative() {

		List<Employee> employees = new ArrayList<Employee>();
		Employee employee=new Employee(-5190, "jansi", "db", "jansi@gmail.com");
		employees.add(employee);
		Mockito.when(employeeService.getAllEmployess()).thenReturn(employees);
		ResponseEntity<List<Employee>> re = employeeeController.getAllEmployess();
		Assert.assertNotNull(re);
		Assert.assertEquals(HttpStatus.OK, re.getStatusCode());

	}

	@Test
	public void testUpdateStudentForPositive() {
		Employee employee=new Employee(5190, "jansi", "db", "jansi@gmail.com");
		EmployeeDto dto=new EmployeeDto();
		BeanUtils.copyProperties(employee, dto);
		Mockito.when(employeeService.updateEmployee(dto)).thenReturn(employee);
		ResponseEntity<Employee> employee1 = employeeeController.updateemployee(dto);
		Assert.assertNotNull(employee1);
		Assert.assertEquals(HttpStatus.OK, employee1.getStatusCode());

	}

	@Test
	public void testUpdateStudentForNegative() {
		Employee employee=new Employee(-5190, "jansi", "db", "jansi@gmail.com");
		EmployeeDto dto=new EmployeeDto();
		BeanUtils.copyProperties(employee, dto);
		Mockito.when(employeeService.updateEmployee(dto)).thenReturn(employee);
		ResponseEntity<Employee> employee1 = employeeeController.updateemployee(dto);
		Assert.assertNotNull(employee1);
		Assert.assertEquals(HttpStatus.OK, employee1.getStatusCode());

	}



}
