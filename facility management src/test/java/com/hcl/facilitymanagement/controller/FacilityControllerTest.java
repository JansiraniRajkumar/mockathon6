package com.hcl.facilitymanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.facilitymanagement.dto.FacilityDto;
import com.hcl.facilitymanagement.dto.ResponseDto;
import com.hcl.facilitymanagement.model.Facility;
import com.hcl.facilitymanagement.service.FacilityService;

@RunWith(MockitoJUnitRunner.Silent.class)

public class FacilityControllerTest {
	@InjectMocks
	FacilityController facilityController;

	@Mock
	FacilityService facilityService;

	@Test
	public void createFacilityTest() {
		ResponseDto responseflight = new ResponseDto();
		responseflight.setMessage("success");

		FacilityDto f = new FacilityDto();
		f.setFacilityId(1);
		f.setFacilityName("yerwada");
		f.setFacilityCity("pune");
		f.setFacilityName("hcl");
		Mockito.when(facilityService.createFacility(f)).thenReturn(responseflight);

		ResponseEntity<ResponseDto> result = facilityController.createFacility(f);

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void updateFacilityTest() {

		Facility facility = new Facility();

		FacilityDto f = new FacilityDto();
		f.setFacilityId(1);
		f.setFacilityName("yerwada");
		f.setFacilityCity("pune");
		f.setFacilityName("hcl");
		Mockito.when(facilityService.updateFacility(f)).thenReturn(facility);

		ResponseEntity<Facility> result = facilityController.updateFacility(f);

		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	public void deleteFacilityTest() {
		Mockito.when(facilityService.deleteById(1)).thenReturn(null);
		ResponseEntity<ResponseDto> result = facilityController.deleteFacility(1);

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void getFacilityTest() {
		Facility f = new Facility();
		f.setFacilityId(1);
		f.setFacilityCity("pune");
		f.setFacilityName("sai");
		Mockito.when(facilityService.getById(1)).thenReturn(f);
		ResponseEntity<Facility> result = facilityController.getFacility(1);

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}
	@Test
	
	public void getAllFacilitiesTest() {
		List<Facility> f = new ArrayList<Facility>();
		Facility facility = new Facility();
		facility.setFacilityId(1);
		facility.setFacilityCity("pine");
		facility.setFacilityName("mounika");
		f.add(facility);
		Facility facility1 = new Facility();
		facility1.setFacilityId(1);
		facility1.setFacilityCity("pine");
		facility1.setFacilityName("mounika");
		f.add(facility1);
		
		Mockito.when(facilityService.getAllFacilities()).thenReturn(f);
		ResponseEntity<List<Facility>> result = facilityController.getAllFacilities();

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}
	
}