package com.hcl.facilitymanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.facilitymanagement.dao.FacilityRepository;
import com.hcl.facilitymanagement.dto.FacilityDto;
import com.hcl.facilitymanagement.dto.ResponseDto;
import com.hcl.facilitymanagement.model.Facility;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)

public class FacilityServiceImplTest {
	@InjectMocks
	FacilityServiceImpl facilityServiceImpl;

	@Mock
	FacilityRepository facilityRepository;

	@Test
	public void createFacility() {
		FacilityDto facilityDto = new FacilityDto();
		facilityDto.setFacilityBranch("pune");
		facilityDto.setFacilityCity("mumbai");
		facilityDto.setFacilityId(1);
		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("created successfully");
		Facility facility = new Facility();

		Mockito.when(facilityRepository.save(facility)).thenReturn(facility);
		ResponseDto resStudent = facilityServiceImpl.createFacility(facilityDto);

		Assert.assertNotNull(resStudent);
		Assert.assertEquals("created successfully", resStudent.getMessage());
	}

	@Test
	public void updateFacility() {
		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("created successfully");

		FacilityDto facilityDto = new FacilityDto();
		facilityDto.setFacilityBranch("pune");
		facilityDto.setFacilityCity("mumbai");
		facilityDto.setFacilityId(1);
		Facility facility = new Facility();
		facility.setFacilityBranch("puune");
		facility.setFacilityCity("mumbai");
		facility.setFacilityId(1);
		Mockito.when(facilityRepository.save(facility)).thenReturn(facility);
		Facility resStudent = facilityServiceImpl.updateFacility(facilityDto);
		Assert.assertEquals("mumbai", facility.getFacilityCity());

	}

	@Test
	public void getAllFacilities() {
		List<Facility> f = new ArrayList<Facility>();
		Facility facility = new Facility();
		facility.setFacilityId(1);
		facility.setFacilityCity("pine");
		facility.setFacilityName("mounika");
		f.add(facility);
		Facility facility1 = new Facility();
		facility1.setFacilityId(1);
		facility1.setFacilityCity("pine");
		facility1.setFacilityName("mounika");
		f.add(facility1);

		Mockito.when(facilityRepository.findAll()).thenReturn(f);
		List<Facility> resStudent = facilityServiceImpl.getAllFacilities();
		Assert.assertNotNull(resStudent);
		Assert.assertEquals(2, f.size());
}
	@Test
	public void getByIdforpositive() {
		Facility facility= new Facility();
		facility.setFacilityId(1);
		facility.setFacilityCity("pine");
		facility.setFacilityName("mounika");

		Mockito.when(facilityRepository.getOne(1)).thenReturn(facility);
		Facility resStudent = facilityServiceImpl.getById(1);
		Assert.assertNotNull(resStudent);
		Assert.assertEquals("pine",facility.getFacilityCity());
	}
	@Test
	public void getByIdforNegative() {
		Facility facility= new Facility();
		facility.setFacilityId(1);
		facility.setFacilityCity("pine");
		facility.setFacilityName("mounika");

		Mockito.when(facilityRepository.getOne(1)).thenReturn(facility);
		Facility resStudent = facilityServiceImpl.getById(1);
		Assert.assertNotNull(resStudent);
		Assert.assertEquals("pine",facility.getFacilityCity());
	}

}