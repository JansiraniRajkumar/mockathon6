package com.hcl.facilitymanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FacilitymanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacilitymanagementApplication.class, args);
	}

}
