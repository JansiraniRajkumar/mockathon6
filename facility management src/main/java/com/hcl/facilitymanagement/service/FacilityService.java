package com.hcl.facilitymanagement.service;

import java.util.List;

import com.hcl.facilitymanagement.dto.FacilityDto;
import com.hcl.facilitymanagement.dto.ResponseDto;
import com.hcl.facilitymanagement.model.Facility;

public interface FacilityService {
	
	
	public ResponseDto createFacility(FacilityDto facilityDto);
	public Facility updateFacility(FacilityDto fasilityDto);
	public ResponseDto deleteById(int id);
	public List<Facility> getAllFacilities();
	public Facility getById(int id);
	

}
