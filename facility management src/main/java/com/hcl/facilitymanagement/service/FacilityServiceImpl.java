package com.hcl.facilitymanagement.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.facilitymanagement.dao.FacilityRepository;
import com.hcl.facilitymanagement.dto.FacilityDto;
import com.hcl.facilitymanagement.dto.ResponseDto;
import com.hcl.facilitymanagement.model.Facility;
@Service
public class FacilityServiceImpl implements FacilityService{
	
	@Autowired
	private FacilityRepository facilityRepository;
	
	Facility facility=new Facility();
	ResponseDto dto=new ResponseDto();

	@Override
	public ResponseDto createFacility(FacilityDto facilityDto) {
		BeanUtils.copyProperties(facilityDto, facility);
		facility=facilityRepository.save(facility);
		dto.setMessage("created successfully");
		return dto;
		
	}

	@Override
	public Facility updateFacility(FacilityDto facilityDto) {
		BeanUtils.copyProperties(facilityDto, facility);
		Facility facility1=facilityRepository.save(facility);
		return facility1;
	}

	@Override
	public ResponseDto deleteById(int id) {
		facilityRepository.deleteById(id);
		dto.setMessage("deleted successfully");
		return dto;
	}

	@Override
	public List<Facility> getAllFacilities() {

		return facilityRepository.findAll();
	}

	@Override
	public Facility getById(int id) {
		return facilityRepository.getOne(id);
	}

}
