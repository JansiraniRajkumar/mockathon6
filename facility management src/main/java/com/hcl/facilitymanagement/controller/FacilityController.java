package com.hcl.facilitymanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.facilitymanagement.dto.FacilityDto;
import com.hcl.facilitymanagement.dto.ResponseDto;
import com.hcl.facilitymanagement.model.Facility;
import com.hcl.facilitymanagement.service.FacilityService;

@RestController
public class FacilityController {
	 @Autowired
	private FacilityService facilityService;
	 @PostMapping("/facility")
  public ResponseEntity<ResponseDto> createFacility(@RequestBody FacilityDto facilityDto){
	  ResponseDto facility=facilityService.createFacility(facilityDto);
	  return new ResponseEntity<ResponseDto>(facility, HttpStatus.OK);
  }
  
	 @PutMapping("/facility")
	  public ResponseEntity<Facility> updateFacility(@RequestBody FacilityDto facilityDto){
		  Facility facility=facilityService.updateFacility(facilityDto);
		  return new ResponseEntity<Facility>(facility, HttpStatus.OK);
	  }
	 
	 @DeleteMapping("/facility/{id}")
	 public ResponseEntity<ResponseDto>  deleteFacility(@PathVariable int id){
		 ResponseDto facility=facilityService.deleteById(id);
		 return new ResponseEntity<ResponseDto>(facility, HttpStatus.OK);
		 
	 }
	 
	 @GetMapping("/facility/{id}")
	 public ResponseEntity<Facility>  getFacility(@PathVariable int id){
		Facility facility=facilityService.getById(id);
		 return new ResponseEntity<Facility>(facility, HttpStatus.OK);
	 }
	 
	 @GetMapping("/facility")
	 public ResponseEntity<List<Facility>> getAllFacilities(){
		 List<Facility> facilities=facilityService.getAllFacilities();
		 return new ResponseEntity<List<Facility>>(facilities, HttpStatus.OK);
	 }
	 
	 
}
