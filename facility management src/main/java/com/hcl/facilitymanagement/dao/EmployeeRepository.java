package com.hcl.facilitymanagement.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.facilitymanagement.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
