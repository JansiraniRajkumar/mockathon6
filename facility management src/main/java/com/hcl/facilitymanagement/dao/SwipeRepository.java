package com.hcl.facilitymanagement.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.facilitymanagement.model.Swipe;

public interface SwipeRepository extends JpaRepository<Swipe, Integer>{

}
