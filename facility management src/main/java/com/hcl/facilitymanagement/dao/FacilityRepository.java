package com.hcl.facilitymanagement.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.facilitymanagement.model.Facility;

public interface FacilityRepository extends JpaRepository<Facility, Integer>{

}
