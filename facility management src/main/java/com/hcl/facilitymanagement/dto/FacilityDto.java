package com.hcl.facilitymanagement.dto;



public class FacilityDto {

	private int facilityId;
	private String facilityName;
	private String facilityBranch;
	private String facilityCity;
	public int getFacilityId() {
		return facilityId;
	}
	public void setFacilityId(int facilityId) {
		this.facilityId = facilityId;
	}
	public String getFacilityName() {
		return facilityName;
	}
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	public String getFacilityBranch() {
		return facilityBranch;
	}
	public void setFacilityBranch(String facilityBranch) {
		this.facilityBranch = facilityBranch;
	}
	public String getFacilityCity() {
		return facilityCity;
	}
	public void setFacilityCity(String facilityCity) {
		this.facilityCity = facilityCity;
	}
	
	
	
	
}
