package com.hcl.swipemanagement.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.swipemanagement.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
