package com.hcl.swipemanagement.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.swipemanagement.model.Employee;
import com.hcl.swipemanagement.model.Swipe;


public interface SwipeRepository extends JpaRepository<Swipe, Integer>{
	

	List<Swipe> findByDateAndEmployee(Date date, Employee employee);

}
