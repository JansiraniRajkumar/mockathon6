package com.hcl.swipemanagement.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table
public class Swipe {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int swipeId;
	@Temporal(TemporalType.TIMESTAMP)
	private Date swipeInTime;
	@Temporal(TemporalType.TIMESTAMP)
	private Date swipeOutTime;
	
	private double totalWorkingHours;
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	@JoinColumn(name = "employeeCode")
	@JsonIgnore
	private Employee employee;
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	@JoinColumn(name = "facilityId")
	@JsonIgnore
	private Facility facility;
	public int getSwipeId() {
		return swipeId;
	}
	public void setSwipeId(int swipeId) {
		this.swipeId = swipeId;
	}
	
	public Swipe(Date swipeInTime, Date swipeOutTime, double totalWorkingHours, Date date, Employee employee,
			Facility facility) {
		super();
		this.swipeInTime = swipeInTime;
		this.swipeOutTime = swipeOutTime;
		this.totalWorkingHours = totalWorkingHours;
		this.date = date;
		this.employee = employee;
		this.facility = facility;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Facility getFacility() {
		return facility;
	}
	public void setFacility(Facility facility) {
		this.facility = facility;
	}
	public Swipe() {
		super();
	}
	public Date getSwipeInTime() {
		return swipeInTime;
	}
	public void setSwipeInTime(Date swipeInTime) {
		this.swipeInTime = swipeInTime;
	}
	public Date getSwipeOutTime() {
		return swipeOutTime;
	}
	public void setSwipeOutTime(Date swipeOutTime) {
		this.swipeOutTime = swipeOutTime;
	}
	public double getTotalWorkingHours() {
		return totalWorkingHours;
	}
	public void setTotalWorkingHours(double totalWorkingHours) {
		this.totalWorkingHours = totalWorkingHours;
	}
	public Swipe(Date swipeInTime, Date swipeOutTime, double totalWorkingHours, Employee employee, Facility facility) {
		super();
		this.swipeInTime = swipeInTime;
		this.swipeOutTime = swipeOutTime;
		this.totalWorkingHours = totalWorkingHours;
		this.employee = employee;
		this.facility = facility;
	}
	
	
	
	
	
	

}
