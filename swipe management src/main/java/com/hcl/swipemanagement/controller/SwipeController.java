package com.hcl.swipemanagement.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.hcl.swipemanagement.dto.ResponseDto;
import com.hcl.swipemanagement.dto.SwipeDto;
import com.hcl.swipemanagement.dto.SwipeOutDto;
import com.hcl.swipemanagement.exception.InsufficientDataException;
import com.hcl.swipemanagement.exception.InvalidDetailsException;
import com.hcl.swipemanagement.exception.NoFacilityDataFoundException;
import com.hcl.swipemanagement.exception.SwipeReportFailedException;
import com.hcl.swipemanagement.model.Employee;
import com.hcl.swipemanagement.model.Facility;
import com.hcl.swipemanagement.service.SwipeService;

@RestController
public class SwipeController {
	@Autowired
	SwipeService swipeService;
	 
	   
	
	@PostMapping("/swipeIn")
	public ResponseEntity<ResponseDto> swipeIn(@RequestBody SwipeDto swipeDto) throws InsufficientDataException, InvalidDetailsException {
		ResponseDto responseDto=new ResponseDto();
		swipeService.saveSwipeIn(swipeDto);
		responseDto.setMessage("swipeIn saved successfully");
		
		responseDto.getMessage();
		return new ResponseEntity<ResponseDto>(responseDto, HttpStatus.OK);	
	}
	@PostMapping("/swipeOut")
	public ResponseEntity<ResponseDto> swipeOut(@RequestBody SwipeOutDto swipeOutDto) throws InsufficientDataException, InvalidDetailsException {
		ResponseDto responseDto=new ResponseDto();
		swipeService.saveSwipeOut(swipeOutDto);
		responseDto.setMessage("swipeOut saved successfully");
		
		responseDto.getMessage();
		return new ResponseEntity<ResponseDto>(responseDto, HttpStatus.OK);	
	}

	@GetMapping("/swipeReportByEmployee")
	public ResponseEntity<String> getSwipeReportByEmployeeId(@RequestParam long id) throws SwipeReportFailedException {
		ResponseDto responseDto=new ResponseDto();
		String swipe=swipeService.getSwipeEmployee(id);
		responseDto.setMessage("swipe saved successfully");
		responseDto.getMessage();
		
		return new ResponseEntity<>(swipe, HttpStatus.OK);	
	}
	@GetMapping("/swipeReportByFacility")
	public ResponseEntity<Facility> getSwipeReportByFacility(@RequestParam int id) throws SwipeReportFailedException, NoFacilityDataFoundException {
		ResponseDto responseDto=new ResponseDto();
		Facility swipe=swipeService.getSwipeByFaciltyId(id);
		responseDto.setMessage("swipe saved successfully");
		responseDto.getMessage();
		return new ResponseEntity<>(swipe, HttpStatus.OK);	
	}
}
