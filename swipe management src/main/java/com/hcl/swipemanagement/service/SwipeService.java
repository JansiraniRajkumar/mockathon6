package com.hcl.swipemanagement.service;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.hcl.swipemanagement.dao.EmployeeRepository;
import com.hcl.swipemanagement.dao.SwipeRepository;
import com.hcl.swipemanagement.dto.SwipeDto;
import com.hcl.swipemanagement.dto.SwipeOutDto;
import com.hcl.swipemanagement.exception.InsufficientDataException;
import com.hcl.swipemanagement.exception.InvalidDetailsException;
import com.hcl.swipemanagement.exception.NoEmployeeDataFoundException;
import com.hcl.swipemanagement.exception.NoFacilityDataFoundException;
import com.hcl.swipemanagement.exception.SwipeReportFailedException;
import com.hcl.swipemanagement.model.Employee;
import com.hcl.swipemanagement.model.Facility;
import com.hcl.swipemanagement.model.Swipe;

@Service
public class SwipeService {
	@Autowired
	SwipeRepository swipeRepository;

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	RestTemplate restTemplate;

	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	public Swipe saveSwipeIn(SwipeDto swipeDto) throws InsufficientDataException, InvalidDetailsException {
		Swipe swipe = new Swipe();
		if (swipeDto.getSwipeIn() != null && swipeDto.getEmployeeId() != 0 && swipeDto.getFacilityId() != 0) {
			
			Employee employee = new Employee();
			employee.setEmployeeCode(swipeDto.getEmployeeId());
			Facility facility = new Facility();
			facility.setFacilityId(swipeDto.getFacilityId());
			swipe.setEmployee(employee);
			swipe.setFacility(facility);
			swipe.setSwipeInTime(swipeDto.getSwipeIn());
			swipe.setDate(new Date());
			try {
				swipeRepository.save(swipe);
			}catch(Exception e) {
				throw new InvalidDetailsException("invalid employee id or facility id");
			}
			
		} else {
			throw new InsufficientDataException("please fill the all the Details");
		}
		return swipe;
	}
	public Swipe saveSwipeOut(SwipeOutDto swipeOutDto) throws InsufficientDataException, InvalidDetailsException {
		Optional<Swipe> s;
		if (swipeOutDto.getEmployeeId() != 0 && swipeOutDto.getFacilityId() != 0&&swipeOutDto.getSwipeOut()!=null) {
			
			Employee employee = employeeRepository.findById(swipeOutDto.getEmployeeId()).orElseThrow(()->new NoEmployeeDataFoundException());
			List<Swipe> swipe=swipeRepository.findByDateAndEmployee(new Date(),employee);
			 s=swipe.stream().sorted(Comparator.comparing(Swipe::getSwipeId).reversed()).findFirst();
			long t1=swipeOutDto.getSwipeOut().getTime()/100000;
			long t2=s.get().getSwipeInTime().getTime();
			long t3=t1-t2;
			s.get().setSwipeOutTime(swipeOutDto.getSwipeOut());
			s.get().setTotalWorkingHours(t3/(60*60*1000));
			try {
				swipeRepository.save(s.get());
			}catch(Exception e) {
				throw new InvalidDetailsException("invalid employee id or facility id");
			}
	
		} else {
			throw new InsufficientDataException("please fill the all the Details");
		}
		return s.get();
	}
	

	public String getSwipeEmployee(Long id) throws SwipeReportFailedException {
		try {
               
			String result = restTemplate.getForObject("http://EMPLOYEE-SERVICE/employee/" + id, String.class);
			
			return result;
		} catch (HttpClientErrorException e) {
			throw new NoEmployeeDataFoundException();
		}
	}

	public Facility getSwipeByFaciltyId(int id) throws SwipeReportFailedException, NoFacilityDataFoundException {
		try {
			
			Facility result = restTemplate.getForObject("http://FACILITY-SERVICE/facility/" + id, Facility.class);
			
			return result;
		} catch (HttpClientErrorException e) {
			throw new NoFacilityDataFoundException();
		}
	}

}
