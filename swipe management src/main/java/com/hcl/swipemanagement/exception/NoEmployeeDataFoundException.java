package com.hcl.swipemanagement.exception;

@SuppressWarnings("serial")
public class NoEmployeeDataFoundException extends RuntimeException{
	public NoEmployeeDataFoundException() {
		super("NoEmployeeFoundException");
	}

}
