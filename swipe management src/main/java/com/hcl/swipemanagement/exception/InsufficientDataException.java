package com.hcl.swipemanagement.exception;

public class InsufficientDataException extends Exception {

	private static final long serialVersionUID = 1L;

	public InsufficientDataException(String string) {
		super(string);
	}

}
