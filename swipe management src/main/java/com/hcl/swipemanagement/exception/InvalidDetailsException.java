package com.hcl.swipemanagement.exception;

public class InvalidDetailsException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidDetailsException(String string){
		super(string);
	}
}
