package com.hcl.swipemanagement.exception;

public class SwipeReportFailedException extends Exception {

	private static final long serialVersionUID = 1L;

	public SwipeReportFailedException(String string) {
		super(string);
	}

}
