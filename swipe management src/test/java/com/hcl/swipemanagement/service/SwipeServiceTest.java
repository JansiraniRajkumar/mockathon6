package com.hcl.swipemanagement.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner.Silent;

import com.hcl.swipemanagement.dao.SwipeRepository;
import com.hcl.swipemanagement.model.Swipe;


@RunWith(Silent.class)
public class SwipeServiceTest {
	@InjectMocks
	SwipeService swipeService;
	@Mock
	SwipeRepository swipeRepository;

	@Test

	public void testSwipeCreateForPositive() {
		List<Swipe> swipes = new ArrayList<Swipe>();
		Swipe swipe = new Swipe();
		swipe.setSwipeId(1);
		swipe.setSwipeInTime(null);
		swipes.add(swipe);
		Mockito.when(swipeRepository.save(swipe)).thenReturn(swipe);
	}

	@Test
	public void testSwipeCreateForNegative() {
		List<Swipe> swipes = new ArrayList<Swipe>();
		Swipe swipe = new Swipe();
		swipe.setSwipeId(-1);
		swipe.setSwipeInTime(null);
		swipes.add(swipe);
		Mockito.when(swipeRepository.save(swipe)).thenReturn(swipe);
	}

}